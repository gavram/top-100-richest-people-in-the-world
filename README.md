# Top 100 Richest People in the World

This is an analysis of the total wealth of the 100 richest people in the world. Available data are for the period 2001 to 2019.


## Data

Data is obtained from https://www.forbes.com
There are no missing values in the dataset and it is very consistent

## Preparation
Data is obtained using process.py script from the aforementioned web address. Script uses web API query to get data.
The data is originally in JSON format and the script transforms, cleans and filters the data into a CSV file


## License
Usage of this data is prescribed by Forbes Terms and Conditions https://www.forbes.com/terms-and-conditions
Python scripts are free to use and distribute CC0-1.0 https://creativecommons.org/publicdomain/zero/1.0/

## Total Net Worth by Year - Chart
![](/img/chart.png)