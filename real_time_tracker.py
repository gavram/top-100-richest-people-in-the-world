import requests
import json
import time
import datetime
import csv
import os


# prepare requests header
headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36\
     (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
    'referer': 'https://www.forbes.com/billionaires/list/'
}

# setting up current year and CSV header
now = datetime.datetime.now()
year = now.year
header_row = ['year', 'position', 'rank', 'name', 'worth', 'age', 'industry', 'gender', 'country']

# create CSV file to store data
with open('./data/real_time_data.csv','w', newline='') as csvfile:    
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow([g for g in header_row]) 

    # get, transform and filter data
    year = str(year)
    print(year + ' billionaires!')
    res = requests.get('https://www.forbes.com/ajax/list/data?year='\
         + year +'&uri=billionaires&type=person', headers=headers)
    time.sleep(10)
    data = res.text

    # store data to temporary json file
    with open('./data/data.json', 'w') as f:
        f.write(res.text)
        f.close
    with open("./data/data.json") as file:
        rows = json.load(file)

        # transform, filter and store data from data.json to csv file
        for row in rows:
            if row.get('position', 10000) <= 100:
                list_row = [year, 1, 2, 3, 4, 5, 6, 7,8]
                list_row[list_row.index(1)] = row.get('position')
                list_row[list_row.index(2)] = row.get('rank')
                list_row[list_row.index(3)] = row.get('name')
                list_row[list_row.index(4)] = round(row.get('worth')/1000, 2)
                list_row[list_row.index(5)] = row.get('age')
                list_row[list_row.index(6)] = row.get('industry')
                list_row[list_row.index(7)] = row.get('gender')
                list_row[list_row.index(8)] = row.get('country')
                writer.writerow(list_row)

# delete data.json
if os.path.exists('./data/data.json'):
    os.remove('./data/data.json')     